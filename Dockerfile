FROM python:3.6

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


COPY requirements.txt .
RUN pip install -r ./requirements.txt

RUN mkdir /django
COPY ./django_project /django/django_project/
WORKDIR /django/django_project/

