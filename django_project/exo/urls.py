from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import views

informationsUsers = views.InformationsUsers()

app_name = 'exo'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('connexion', informationsUsers.connexion, name='connexion'),
    path('connexion2', informationsUsers.modificationInformationEmail, name='connexion2'),
]


urlpatterns += staticfiles_urlpatterns()