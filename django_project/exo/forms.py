from django import forms

class UserForm(forms.Form):
    user_name = forms.CharField(max_length=100, label="Nom de l'utilisateur")
    user_psw = forms.CharField(max_length=100, widget=forms.PasswordInput, label='Mot de passe')
    verif = forms.BooleanField(label="Cochez si vous souhaitez créer un utilisateur.", required=False)