from django.shortcuts import render
from django.http import JsonResponse
from django.views.generic import TemplateView
import json

from .models import Users
from .forms import UserForm

class IndexView(TemplateView):
    template_name = './exo/index.html'
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['form'] = UserForm(self.request.POST or None)
        context['error'] = False
        return context
    

class InformationsUsers():

    def __init__(self):
        super().__init__()

    def connexion(self, request):
        if request.method  == 'POST':
            form = UserForm(request.POST)
            if form.is_valid():
                user_name = form.cleaned_data['user_name']
                #Check id ?
                if not form.cleaned_data['verif']:
                    #User exist ? 
                    if Users.objects.filter(user_name=user_name, user_psw=form.cleaned_data['user_psw']).exists():
                       return self.connexionVerificationValid(request,user_name)
                    #User not exist
                    else:
                        return self.connexionVerificationInValid(request)
                #no Check ? => Create or get User
                else:
                    return self.connexionWithoutPswValidation(request, user_name, form.cleaned_data['user_psw'])

    def connexionVerificationValid(self, request, user_name):
        print("connexionVerificationValid", user_name)
        user = Users.objects.get(user_name=user_name); user.user_psw = None
        return render(request, './exo/modal.html', {'user': user, 'connexionType': 'Connexion avec vérification de mot de passe'} )
    
    def connexionVerificationInValid(self, request):
        print("connexionVerificationInValid")
        form = UserForm(request.POST or None)
        return render(request, './exo/index.html', {'form': form, 'error': True})
    
    def connexionWithoutPswValidation(self, request, user_name, user_psw):
        print("connexionWithoutPswValidation")
        connexionType = "Connexion sans vérification de mot de passe, "
        #User exist ? 
        if Users.objects.filter(user_name=user_name).exists() :
            #Get user
            user = Users.objects.get(user_name=user_name)
            connexionType += "pas de création d'utilisateur"
        else:
            #create user
            user, created = Users.objects.get_or_create(user_name=user_name, user_psw=user_psw)
            connexionType += "avec création d'utilisateur"
        #On ne transmet pas le psw au font
        user.user_psw = None
        return render(request, './exo/modal.html', {'user': user, 'connexionType': connexionType})

    def modificationInformationEmail(self, request):
        print("modificationInformationEmail")
        if request.method  == 'GET':
            print("DATA: ", request.GET)
            #Check user exist
            if Users.objects.filter(user_name=request.GET['user_name']).exists():
                user = Users.objects.get(user_name=request.GET['user_name'])
                user.user_email = request.GET['user_email']
                user.save()
                return JsonResponse({'user_email': user.user_email, 'fait':True})
