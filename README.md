# exo-django

## Les consignes 

Dans l’objectif d’apprendre à utiliser Django, nous aimerions que vous fassiez un site en suivant les instructions suivantes :

-  Nous souhaitons une page permettant à un utilisateur de se connecter.

-  Une fois connecté, l'utilisateur accède à une page pour afficher / modifier ses informations.

- En cliquant sur "Modifier ses informations", on veut avoir une modale qui s'ouvre.

-   Dans cette modale,  on veut un input permettant de modifier son adresse email, ainsi qu'un bouton "enregistrer". A l'appui de ce bouton, l'adresse email sera modifiée dans la base de données et sur la page principale sans rechargement de la page.

-  Le code python doit être écrit en Orienté Objet.

-   On utilisera Django, Python et Bootstrap. Vous êtes libre d'utiliser d'autres outils ou Frameworks selon vos préférences.

-   Nous aimerions le rendu sous Git
Vous devrez utiliser Docker pour le containériser. 
Deadline : lundi 24/08 8h 


## Run and build this project

Faire : 

 1 - git clone https://gitlab.com/nicolascastry/exo-django.git

 2 - cd exo-django

 3 - docker-compose up --build -d

 4 - docker exec -it exo-django_web_1 sh

 5 - python manage.py migrate

 6 - go sur http://localhost:8000/

NB : 
    - Attention le port 8000 ne doit pas être utilisé.
